CXX=clang++
CFLAGS= -Wall -Wextra -Werror -g

SRC_DIR=./src/
LIB_DIR=./lib/

ALL_C=$(wildcard $(SRC_DIR)*.c) $(wildcard $(LIB_DIR)*.c)

SRC=main.cpp ${ALL_C}
OBJ=${SRC:*.c=*.o}

TSRC=unit_tests.cpp ${ALL_C}
TOBJ=${TSRC:*.c=*.o}

all: main unit_tests

main: ${OBJ}

unit_tests: ${TOBJ}

clena: clean #FIXME

clean:
	${RM} main unit_tests *.o *.d
